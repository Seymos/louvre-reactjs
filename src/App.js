import React, { Component } from 'react';
import axios from "axios"
import './App.scss';

class App extends Component {
  state = {
    visitDate: Date(),
    guestNumber: 0,
    entryTypes: ['Journée', 'Demi-journée'],
    entryType: "Journée",
    email: 'example@gmail.com'
  }

  handleChange = (event) => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  handleSubmit = async event => {
    event.preventDefault()
    const { visitDate, guestNumber, entryType, email } = this.state
    console.log('visit day: ',visitDate)
    console.log('guests: ',guestNumber)
    console.log('entry type: ',entryType)
    console.log('email: ',email)

    let postOptions = {
      method: 'POST',
      url: `http://127.0.0.1:8000/api/orderings`,
      data: JSON.stringify({
        visitDate: visitDate,
        ticketNumber: parseInt(guestNumber),
        type: entryType,
        email: email
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      json: true
    }
    await axios(postOptions)
        .then(response => {
          if (response.status === 201) {
            localStorage.setItem('current_ordering',JSON.stringify(response.data))
          }
          console.log('response: ',response);
        })
        .catch(error => {
          console.log('error: ',error);
        });
  }

  render() {
    const { visitDate, guestNumber, entryTypes, email } = this.state
    return (
        <div className="App">
          <h1>Application de réservation pour le musée du Louvre.</h1>
          <form onSubmit={this.handleSubmit}>
            <fieldset>
              <label htmlFor="visitDate">Jour de votre visite</label>
              <input type='date' name='visitDate' value={visitDate} onChange={this.handleChange} placeholder="Jour de votre visite"/>
            </fieldset>
            <fieldset>
              <label htmlFor="guestNumber">Nombre d'invités</label>
              <input type='number' name='guestNumber' value={guestNumber} onChange={this.handleChange} placeholder="Nombre d'invités"/>
            </fieldset>
            <fieldset>
              <label htmlFor="entryType">Type de billet</label>
              <select name="entryType" onChange={this.handleChange}>
                {entryTypes.map((type, index) => {
                  return (
                      <option key={index} value={type}>{type}</option>
                  )
                })}
              </select>
            </fieldset>
            <fieldset>
              <label htmlFor="email">Votre e-mail</label>
              <input type='email' name='email' value={email} onChange={this.handleChange} placeholder="Votre e-mail"/>
            </fieldset>
            <button type='submit'>Etape suivate</button>
          </form>
        </div>
    )
  }
}

export default App;
